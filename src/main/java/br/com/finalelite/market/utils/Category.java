package br.com.finalelite.market.utils;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.Material;

import java.util.Arrays;

@RequiredArgsConstructor
@Getter
public enum Category {
    FOOD("Alimentos", Material.BREAD),
    DROP("Drops", Material.BONE),
    FARM("Plantações", Material.WHEAT),
    SPAWNERS("Spawners", Material.SPAWNER),
    REDSTONE("Redstone", Material.REDSTONE),
    TRIDENTS("Tridentes", Material.TRIDENT),
    BLOCKS("Blocos", Material.BRICKS),
    COLOR("Coloridos", Material.ROSE_RED),
    ORES("Minérios", Material.DIAMOND_ORE),
    POTIONS("Alquimia", Material.BREWING_STAND),
    CHESTS("Caixas", Material.CHEST),
    WAR("Guerra", Material.TNT),
    PANELS("Paineis Soláres", Material.DAYLIGHT_DETECTOR);

    private final String name;
    private final Material material;

    public static Category fromId(int id) {
        if (id > Category.values().length)
            return null;

        return Category.values()[id];
    }

    public static Category getFromName(String name) {
        return Arrays.stream(Category.values()).filter(category -> category.getName().equalsIgnoreCase(name)).findFirst().orElseGet(null);
    }

    public static Category getFromSimpleName(String name) {
        return Arrays.stream(Category.values()).filter(category -> category.getSimpleName().equalsIgnoreCase(name)).findFirst().orElseGet(null);
    }

    public String getSimpleName() {
        return name.split(" ")[0];
    }
}
