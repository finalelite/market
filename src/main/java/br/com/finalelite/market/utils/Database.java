package br.com.finalelite.market.utils;

import com.gitlab.pauloo27.core.sql.*;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class Database {

    private final EzSQL sql;

    private EzTable market;
    private EzTable marketLog;

    public void connect() throws SQLException {
        sql.connect();

        market = sql.createIfNotExists(
                new EzTableBuilder("market")
                        .withColumn(new EzColumnBuilder("id", EzDataType.PRIMARY_KEY))
                        .withColumn(new EzColumnBuilder("category", EzDataType.INTEGER))
                        .withColumn(new EzColumnBuilder("material", EzDataType.VARCHAR, 64, EzAttribute.NOT_NULL))
                        .withColumn(new EzColumnBuilder("name", EzDataType.VARCHAR, 64))
                        .withColumn(new EzColumnBuilder("lore", EzDataType.VARCHAR, 64))
                        .withColumn(new EzColumnBuilder("amount", EzDataType.INTEGER))
                        .withColumn(new EzColumnBuilder("removed", EzDataType.BOOLEAN)
                                .withDefaultValue(false))
                        .withColumn(new EzColumnBuilder("buyPrice", EzDataType.FLOAT))
                        .withColumn(new EzColumnBuilder("sellPrice", EzDataType.FLOAT))
                        .withColumn(new EzColumnBuilder("energy", EzDataType.FLOAT))
        );

        marketLog = sql.createIfNotExists(
                new EzTableBuilder("market_log")
                        .withColumn(new EzColumnBuilder("id", EzDataType.PRIMARY_KEY))
                        .withColumn(new EzColumnBuilder("tradeType", EzDataType.BOOLEAN))
                        .withColumn(new EzColumnBuilder("productId", EzDataType.INTEGER, EzAttribute.NOT_NULL))
                        .withColumn(new EzColumnBuilder("uuid", EzDataType.VARCHAR, 36, EzAttribute.NOT_NULL))
                        .withColumn(new EzColumnBuilder("date", EzDataType.TIMESTAMP, EzAttribute.NOT_NULL))
                        .withColumn(new EzColumnBuilder("amount", EzDataType.INTEGER, EzAttribute.NOT_NULL))
        );
    }

    public Product buildProduct(ResultSet rs) throws SQLException {
        return new Product(rs.getInt("id"),
                Category.fromId(rs.getInt("category")),
                Material.valueOf(rs.getString("material")),
                rs.getString("name"),
                rs.getBoolean("removed"),
                rs.getString("lore") == null ? null : Arrays.stream(rs.getString("lore").split("\n")).collect(Collectors.toList()),
                rs.getInt("amount"),
                rs.getDouble("energy"),
                rs.getDouble("buyPrice"),
                rs.getDouble("sellPrice")
        );
    }

    public Product getProductById(int id) throws SQLException {
        try (val rs = market
                .select(new EzSelect("*")
                        .where().equals("id", id).and().equals("removed", false))
                .getResultSet()) {
            if (rs.next())
                return buildProduct(rs);
        }
        return null;
    }

    public int updateProductPrice(Product product) throws SQLException {
        try (val result = market.update(new EzUpdate()
                .set("energy", product.getEnergy())
                .set("buyPrice", product.getBuyPrice())
                .set("sellPrice", product.getSellPrice())
                .where().equals("id", product.getId())                
            )) {
            return result.getUpdatedRows();
        }
    }

    public double getEnergyByPlayer(String uuid) {
        val table = sql.getTable("Energia_data");

        try (val result = table.select(new EzSelect("Energia").where().equals("UUID", uuid)).getResultSet()) {
            if (result.next())
                return result.getDouble("Energia");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public void withdrawEnergyToPlayer(String uuid, double energy) {
        try (val stmt = sql.getConnection().prepareStatement("UPDATE Energia_data SET Energia = Energia - ? WHERE UUID = ?")) {
            stmt.setDouble(1, energy);
            stmt.setString(2, uuid);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void withdrawEnergyToPlayer(Player player, double energy) {
        withdrawEnergyToPlayer(player.getUniqueId().toString(), energy);
    }

    public double getEnergyByPlayer(Player player) {
        return getEnergyByPlayer(player.getUniqueId().toString());
    }

    public List<Product> getProductsByCategory(Category category) throws SQLException {
        val list = new ArrayList<Product>();
        try (val rs = market.select(new EzSelect("*")
                .where().equals("category", category.ordinal()).and().equals("removed", false)).getResultSet()) {
            while (rs.next())
                list.add(buildProduct(rs));
        }
        return list;
    }

    public void deleteProduct(Product product) throws SQLException {
        market.update(new EzUpdate()
                .set("removed", true)
                .where().equals("id", product.getId())).close();
    }

    public void insertProduct(Product product) throws SQLException {
        if (product.getLore() != null) {
            market.insertAndClose(
                    new EzInsert(
                            "material, category, name, lore, amount, energy, buyPrice, sellPrice",
                            product.getMaterial().name(),
                            product.getCategory().ordinal(),
                            product.getName(),
                            String.join("\n", product.getLore()),
                            product.getAmount(),
                            product.getEnergy(),
                            product.getBuyPrice(),
                            product.getSellPrice()));
        } else {
            market.insertAndClose(new EzInsert("material, category, name, amount, energy, buyPrice, sellPrice",
                    product.getMaterial().name(),
                    product.getCategory().ordinal(),
                    product.getName(),
                    product.getAmount(),
                    product.getEnergy(),
                    product.getBuyPrice(),
                    product.getSellPrice()));
        }
    }

    public void logTrade(Player player, Product product, TradeType tradeType) throws SQLException {
        marketLog.insertAndClose(
                new EzInsert(
                        "productId, tradeType, uuid, date, amount",
                        product.getId(),
                        tradeType.ordinal(),
                        player.getUniqueId().toString(),
                        new Date(),
                        product.getAmount()
                )
        );
    }
}
