package br.com.finalelite.market.utils;

import br.com.finalelite.pauloo27.api.PauloAPI;
import br.com.finalelite.pauloo27.api.database.PlayerGuild;
import br.com.finalelite.pauloo27.api.database.PlayerRole;
import lombok.*;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@AllArgsConstructor
@Builder
@Setter
@Getter
public class Product {
    private int id;
    private Category category;
    private Material material;
    private String name;
    private boolean removed;
    private List<String> lore;
    private int amount;
    private double energy;
    private double buyPrice;
    private double sellPrice;

    private static final Map<PlayerRole, Double> sellVipDiscount = new HashMap<>();
    private static final Map<PlayerRole, Double> buyVipDiscount = new HashMap<>();


    static {
        buyVipDiscount.put(PlayerRole.CONDE, -3.0);
        buyVipDiscount.put(PlayerRole.LORD, -5.0);
        buyVipDiscount.put(PlayerRole.DUQUE, -8.0);
        buyVipDiscount.put(PlayerRole.TITAN, -10.0);

        sellVipDiscount.put(PlayerRole.CONDE, +3.0);
        sellVipDiscount.put(PlayerRole.LORD, +5.0);
        sellVipDiscount.put(PlayerRole.DUQUE, +8.0);
        sellVipDiscount.put(PlayerRole.TITAN, +10.0);
    }

    public double getBuyPriceByPlayer(Player player) {
        val info = PauloAPI.getInstance().getAccountInfo(player);
        val playerRole = info.getRole();

        val vipDiscount = buyVipDiscount.getOrDefault(playerRole, 0.0);
        val guildDiscount = getBuyGuildDiscount(info.getGuild());

        val totalDiscount = vipDiscount + guildDiscount;

        return buyPrice + (buyPrice / 100) * totalDiscount;
    }

    public double getSellPriceByPlayer(Player player) {
        val info = PauloAPI.getInstance().getAccountInfo(player);
        val playerRole = info.getRole();

        val vipDiscount = sellVipDiscount.getOrDefault(playerRole, 0.0);
        val guildDiscount = getSellGuildDiscount(info.getGuild());

        val totalDiscount = vipDiscount + guildDiscount;

        return sellPrice + (sellPrice / 100) * totalDiscount;
    }

    /*
        Guild: Buy / Sell

        Nobre: -3% / +3%
        Sanguinaria: +3% / -3%
        Ancia: 0% / 0%
     */
    public double getBuyGuildDiscount(PlayerGuild guild) {
        if (guild.isNobre())
            return -3;

        if (guild.isSanguinaria())
            return 3;

        return 0;

    }

    public double getSellGuildDiscount(PlayerGuild guild) {
        if (guild.isNobre())
            return +3;

        if (guild.isSanguinaria())
            return -3;

        return 0;
    }
}
