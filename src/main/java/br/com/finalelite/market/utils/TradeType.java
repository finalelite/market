package br.com.finalelite.market.utils;

public enum TradeType {
    BUY,
    SELL
}
