package br.com.finalelite.market.utils;

import lombok.RequiredArgsConstructor;
import lombok.val;
import org.bukkit.entity.Player;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
public class ProductManager {

    private final Database database;
    private Map<Category, List<Product>> cache;

    public void clearCache() {
        cache = new HashMap<>();
        Arrays.stream(Category.values()).forEach(category -> {
            try {
                cache.put(category, database.getProductsByCategory(category));
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public List<Product> getByCategory(Category category) {
        return cache.get(category);
    }

    public void insertProduct(Product product) throws SQLException {
        database.insertProduct(product);
        cache.put(product.getCategory(), database.getProductsByCategory(product.getCategory()));
    }

    public boolean updateProductPrice(Product product) throws SQLException {
        val result = database.updateProductPrice(product);
        cache.put(product.getCategory(), database.getProductsByCategory(product.getCategory()));
        return result > 0;
    }


    public void deleteProduct(Product product) throws SQLException {
        database.deleteProduct(product);
        cache.put(product.getCategory(), database.getProductsByCategory(product.getCategory()));
    }

    public void logTrade(Player player, Product product, TradeType tradeType) {
        try {
            database.logTrade(player, product, tradeType);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
