package br.com.finalelite.market.commands;

import br.com.finalelite.market.guis.CategoriesGUI;
import br.com.finalelite.pauloo27.api.PauloAPI;
import br.com.finalelite.pauloo27.api.commands.BaseCommand;
import br.com.finalelite.pauloo27.api.commands.usage.DefaultParameterType;
import br.com.finalelite.pauloo27.api.commands.usage.Parameter;
import lombok.val;

import java.util.Collections;

public class MarketOthersCommand extends BaseCommand {
    public MarketOthersCommand() {
        super("market");
        setAliases(Collections.singletonList("lojaabrir"));
        usage.addParameter(new Parameter("jogador", DefaultParameterType.PLAYER, true));

        consoleListener = cmd -> {
            val target = cmd.getArgumentAsPlayer(0);

            PauloAPI.getInstance().getGuiManager().open(target, new CategoriesGUI(CategoriesGUI.GuiType.NORMAL));
            return true;
        };
    }
}
