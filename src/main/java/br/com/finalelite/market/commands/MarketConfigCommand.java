package br.com.finalelite.market.commands;

import br.com.finalelite.market.Main;
import br.com.finalelite.market.guis.CategoriesGUI;
import br.com.finalelite.market.utils.Category;
import br.com.finalelite.market.utils.Product;
import br.com.finalelite.pauloo27.api.PauloAPI;
import br.com.finalelite.pauloo27.api.commands.BaseCommand;
import br.com.finalelite.pauloo27.api.commands.usage.DefaultParameterType;
import br.com.finalelite.pauloo27.api.commands.usage.Parameter;
import lombok.val;
import lombok.var;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Collectors;

public class MarketConfigCommand extends BaseCommand {
    public MarketConfigCommand() {
        super("marketconfig");
        setAliases(Collections.singletonList("lojaconfig"));
        usage.addParameter(new Parameter("ação", DefaultParameterType.STRING, false));
        usage.addParameter(new Parameter("categoria", DefaultParameterType.STRING, false));
        usage.addParameter(new Parameter("compra", DefaultParameterType.DOUBLE, false));
        usage.addParameter(new Parameter("venda", DefaultParameterType.DOUBLE, false));
        usage.addParameter(new Parameter("energia", DefaultParameterType.DOUBLE, false));
        usage.setUsageMessageFormat("&cUse &f/%%s %s&c. Para obter ajuda, use &f/lojaconfig ajuda&c.");

        playerListener = cmd -> {
            val player = cmd.getSender();
            if (!PauloAPI.getInstance().getAccountInfo(player).getRole().isMajorStaff()) {
                cmd.reply("&cVocê não tem permissão.");
                return true;
            }

            if (cmd.hasArgument(0)) {
                val action = cmd.getArgumentAsString(0);

                if (action.equalsIgnoreCase("ajuda")) {
                    cmd.reply("&e/lojaconfig add <categoria> <compra> <venda> - Adiciona um produto a loja.");
                    cmd.reply("&e/lojaconfig add <categoria> <compra> <venda> <energia> - Adiciona um produto a loja na categoria de guerra.");
                    cmd.reply("&e/lojaconfig listar - Lista os ids dos produtos da loja.");
                    cmd.reply("&e/lojaconfig editar <id> <compra> <venda> - Edita os produtos da loja.");
                    cmd.reply("&e/lojaconfig editar <id> <compra> <venda> <energy> - Edita os produtos da loja na categoria de guerra.");
                    cmd.reply("&e/lojaconfig remover - Deleta um produto da loja (pela GUI).");
                    cmd.reply("&e/lojaconfig cache - Limpa o cache da Economia / Conta.");
                } else if (action.equalsIgnoreCase("cache")) {
                    Main.getInstance().getProductManager().clearCache();
                    PauloAPI.getInstance().getEconomyManager().clearCache();
                    PauloAPI.getInstance().getAccountsCache().clearCache();
                    cmd.reply("&aCache limpo.");
                    return true;
                } else if (action.equalsIgnoreCase("remover")) {
                    PauloAPI.getInstance().getGuiManager().open(player, new CategoriesGUI(CategoriesGUI.GuiType.CONFIG));
                } else if (action.equalsIgnoreCase("listar")) {
                    PauloAPI.getInstance().getGuiManager().open(player, new CategoriesGUI(CategoriesGUI.GuiType.LIST));

                } else if (action.equalsIgnoreCase("editar")) {

                    if (!cmd.hasArgument(3)) {
                        return false;
                    }

                    var id = 0;
                    try { 
                        id = Integer.parseInt(cmd.getArgumentAsString(1));
                    } catch (NumberFormatException e) {
                        return false;
                    }

                    val buy = cmd.getArgumentAsDouble(2);
                    val sell = cmd.getArgumentAsDouble(3);

                    if (sell > buy) {
                        cmd.reply("&cO valor de venda não pode ser maior do que o de compra.");
                        return true;
                    }

                    try {
                        val product = Main.getInstance().getDatabase().getProductById(id);

                        if (product.getCategory() == Category.WAR && !cmd.hasArgument(4)) {
                            cmd.reply("&cEnergia é necessária para adicionar um item na categoria gerra.");
                            return false;
                        }

                        product.setBuyPrice(buy);
                        product.setSellPrice(sell);

                        if (product.getCategory() == Category.WAR) {
                            product.setEnergy(cmd.getArgumentAsDouble(4));
                        }

                        if (Main.getInstance().getProductManager().updateProductPrice(product)) {
                            cmd.reply(String.format("&aProduto %s editado com sucesso.",
                                    getName(new ItemStack(product.getMaterial()))));
                        } else {
                            cmd.reply("&cProduto não encontrado.");
                        }

                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                } else if (action.equalsIgnoreCase("add")) {
                    if (!cmd.hasArgument(1) || !cmd.hasArgument(2) || !cmd.hasArgument(3))
                        return false;

                    Category category;
                    try {
                        category = Category.getFromSimpleName(cmd.getArgumentAsString(1));
                    } catch (NullPointerException e) {
                        cmd.reply(String.format("&cCategoria inválida. Lista de categorias: &f%s&c.",
                                Arrays.stream(Category.values()).map(Category::getSimpleName).collect(Collectors.joining(", "))));
                        return true;
                    }

                    val buy = cmd.getArgumentAsDouble(2);
                    val sell = cmd.getArgumentAsDouble(3);

                    val item = player.getInventory().getItemInMainHand();
                    if (item == null || item.getType() == Material.AIR) {
                        cmd.reply("&cSegure o item que deseja adicionar na loja.");
                        return true;
                    }

                    if (buy != 0 && sell > buy) {
                        cmd.reply("&cO valor de venda não pode ser maior do que o de compra.");
                        return true;
                    }

                    if (category == Category.WAR && !cmd.hasArgument(4)) {
                        cmd.reply("&cEnergia é necessária para adicionar um item na categoria gerra.");
                        return false;
                    }

                    val energy = cmd.hasArgument(4) ? cmd.getArgumentAsDouble(4) : -1;

                    val product = Product.builder()
                            .material(item.getType())
                            .amount(item.getAmount())
                            .buyPrice(buy)
                            .sellPrice(sell)
                            .category(category)
                            .energy(energy)
                            .name(item.getItemMeta().getDisplayName())
                            .lore(item.getItemMeta().getLore())
                            .build();

                    try {
                        Main.getInstance().getProductManager().insertProduct(product);
                        cmd.reply("&aProduto &f" + getName(item) + " &aadicionado a loja com sucesso.");
                    } catch (SQLException e) {
                        e.printStackTrace();
                        cmd.reply("&cUm erro ocorreu.");
                    }

                } else {
                    return false;
                }
            } else {
                return false;
            }
            return true;
        };
    }

    public static String getName(ItemStack item) {
        return item.hasItemMeta() && item.getItemMeta().hasDisplayName() ? item.getItemMeta().getDisplayName() :
                PauloAPI.getInstance().getPortugueseLocale().getMaterialName(item.getType());
    }
}
