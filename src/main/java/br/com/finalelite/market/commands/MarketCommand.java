package br.com.finalelite.market.commands;

import br.com.finalelite.market.guis.CategoriesGUI;
import br.com.finalelite.pauloo27.api.PauloAPI;
import br.com.finalelite.pauloo27.api.commands.BaseCommand;

import java.util.Collections;

public class MarketCommand extends BaseCommand {
    public MarketCommand() {
        super("market");
        setAliases(Collections.singletonList("loja"));

        playerListener = cmd -> {
            if (!PauloAPI.getInstance().getAccountInfo(cmd.getSender()).getRole().isVIP()
                    && !PauloAPI.getInstance().getAccountInfo(cmd.getSender()).getRole().isStaff()) {
                cmd.reply("&cVocê precisa ser VIP para utilizar esse comando. Vá até a loja usando o comando &f/ir&c.");
                return true;
            }
            PauloAPI.getInstance().getGuiManager().open(cmd.getSender(), new CategoriesGUI(CategoriesGUI.GuiType.NORMAL));
            return true;
        };
    }
}
