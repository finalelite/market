package br.com.finalelite.market.guis;

import br.com.finalelite.market.utils.Category;
import br.com.finalelite.pauloo27.api.gui.BaseGUI;
import br.com.finalelite.pauloo27.api.gui.GUIAction;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.ChatColor;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

@RequiredArgsConstructor
public class CategoriesGUI extends BaseGUI {

    private final GuiType type;

    @Override
    public Inventory buildInventory(Player player) {
        val inventory = Bukkit.createInventory(null, 2 * 9, ChatColor.RED + "Loja");


        val categories = Category.values();
        IntStream.range(0, Category.values().length).forEach(i -> {
            val category = categories[i];
            addItem(inventory, category.getMaterial(), 1,
                    "&e" + category.getName(), Collections.singletonList("&7Loja de " + category.getName()),
                    GUIAction.updateInventory(new CategoryGUI(category, type))
            );
        });

        return inventory;
    }

    public enum GuiType {
        NORMAL,
        LIST,
        CONFIG
    }
}
