package br.com.finalelite.market.guis;

import br.com.finalelite.market.Main;
import br.com.finalelite.market.commands.MarketConfigCommand;
import br.com.finalelite.market.utils.Category;
import br.com.finalelite.market.utils.Product;
import br.com.finalelite.market.utils.TradeType;
import br.com.finalelite.pauloo27.api.PauloAPI;
import br.com.finalelite.pauloo27.api.database.PlayerCoinType;
import br.com.finalelite.pauloo27.api.gui.BaseGUI;
import br.com.finalelite.pauloo27.api.gui.GUIAction;
import br.com.finalelite.pauloo27.api.gui.GUIClick;
import br.com.finalelite.pauloo27.api.message.MessageUtils;
import br.com.finalelite.pauloo27.api.utils.InventoryUtils;
import lombok.RequiredArgsConstructor;
import lombok.val;
import lombok.var;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;


@RequiredArgsConstructor
public class CategoryGUI extends BaseGUI {

    private final Category category;
    private final CategoriesGUI.GuiType type;
    private static final DecimalFormat formatter;

    static {
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.GERMAN);
        otherSymbols.setDecimalSeparator(',');
        otherSymbols.setGroupingSeparator('.');
        formatter = new DecimalFormat("#,###.00", otherSymbols);
    }

    @Override
    public Inventory buildInventory(Player player) {
        val inv = Bukkit.createInventory(null, 6 * 9, ChatColor.RED + category.getName());

        showPage(player, inv, 0);
        return inv;
    }

    private Inventory showPage(Player player, Inventory inventory, int page) {
        // TODO ???
        List<Integer> usable = Arrays.asList(
                10, 11, 12, 13, 14, 15, 16,
                19, 20, 21, 22, 23, 24, 25,
                28, 29, 30, 31, 32, 33, 34/*,
                37, 38, 39, 40, 41, 42, 43*/
        );

        inventory.clear();

        val products = Main.getInstance().getProductManager().getByCategory(category);

        setItem(inventory, inventory.getSize() - 9, Material.ARROW, 1, "&cVoltar", Arrays.asList("&7Clique para voltar"),
                GUIAction.updateInventory(new CategoriesGUI(type)));

        if (page > 0) {
            setItem(inventory, 48, Material.BLAZE_ROD, 1, "&aPágina anterior", Arrays.asList("&7Clique para ir para a página anterior"),
                    event -> showPage(player, inventory, page - 1));
        }

        val filteredItems = products.stream().skip(usable.size() * page).collect(Collectors.toList());
        if (filteredItems.size() > usable.size()) {
            setItem(inventory, 50, Material.BLAZE_ROD, 1, "&aPróxima página", Arrays.asList("&7Clique para ir para a próxima página"),
                    event -> showPage(player, inventory, page + 1));
        }

        // will be overriden in the future, if not empty :)
        setItem(inventory, usable.get(0), Material.GLASS, 1, "&aEm breve...", null, null);
        AtomicInteger index = new AtomicInteger();
        filteredItems.stream().limit(usable.size()).forEach(product ->
                setItem(
                        inventory,
                        usable.get(index.getAndIncrement()),
                        product.getMaterial(),
                        product.getAmount(),
                        product.getName(),
                        createLore(player, product, type),
                        clickListener(product, type))
        );
        return inventory;
    }

    private static List<String> createLore(Player player, Product product, CategoriesGUI.GuiType type) {
        val lore = new ArrayList<String>();

        if(product.getLore() != null)
            lore.addAll(product.getLore());

        val config = type == CategoriesGUI.GuiType.CONFIG;
        lore.add("");
        lore.add("&aQuantidade &7" + product.getAmount() + "x");
        if (product.getCategory() == Category.WAR) {
            val energy = product.getEnergy();
            if (energy > 0.0) {
                lore.add("&aEnergia &7" + energy + " kW");
            }
        }
        if (type == CategoriesGUI.GuiType.LIST) {
            lore.add("&aID &7" + product.getId());
        }
        lore.add("");


        if (product.getSellPrice() > 0)
            lore.add(String.format("&eBOTÃO ESQUERDO: &cVender &7%dx por $%s", product.getAmount(), formatter.format(product.getSellPriceByPlayer(player))));

        if (product.getBuyPrice() > 0)
            lore.add(String.format("&eBOTÃO DIREITO: &aComprar &7%dx por $%s", product.getAmount(), formatter.format(product.getBuyPriceByPlayer(player))));

        if (product.getSellPrice() > 0 && !config) {
            val count = countItems(player, product);
            if (count > product.getAmount()) {
                val price = (((double) count) / product.getAmount()) * product.getSellPriceByPlayer(player);
                lore.add(String.format("&eBOTÃO ESQUERDO + SHIFT: &cVender &7%dx por $%s", count, formatter.format(price)));
            }
        }
        if (product.getBuyPrice() > 0 && !config && product.getMaterial().getMaxStackSize() > product.getAmount()) {
            val count = product.getMaterial().getMaxStackSize();
            val price = (((double) count) / product.getAmount()) * product.getBuyPriceByPlayer(player);
            lore.add(String.format("&eBOTÃO DIREITO + SHIFT: &aComprar &7%dx por $%s", count, formatter.format(price)));
        }

        if (config) {
            lore.add("");
            lore.add("&eQUALQUER BOTÃO: &c&lAPAGAR");
            lore.add("");
        }

        return lore;
    }

    private static GUIClick clickListener(Product product, CategoriesGUI.GuiType type) {
        return event -> {
            val player = (Player) event.getWhoClicked();
            val click = event.getClick();
            val economyManager = PauloAPI.getInstance().getEconomyManager();
            val economy = economyManager.getPlayerEconomy(player);
            if (type == CategoriesGUI.GuiType.CONFIG) {
                val item = new ItemStack(product.getMaterial());
                val meta = item.getItemMeta();
                meta.setDisplayName(product.getName());

                try {
                    Main.getInstance().getProductManager().deleteProduct(product);
                    MessageUtils.sendColouredMessage(player, String.format("&aProduto &f%s &aremovido da loja&a.",
                            MarketConfigCommand.getName(item)));
                } catch (SQLException e) {
                    player.sendMessage(ChatColor.RED + "Um erro ocorreu.");
                    e.printStackTrace();
                }
                player.closeInventory();
            } else {
                if (click.isLeftClick()) {
                    if (product.getSellPrice() <= 0)
                        return;

                    val count = countItems(player, product);

                    val amount = click.isShiftClick() ? count : product.getAmount();

                    val price = (((double) amount) / product.getAmount()) * product.getSellPriceByPlayer(player);

                    if (product.getAmount() > count) {
                        MessageUtils.sendColouredMessage(player, "&cVocê não tem itens suficientes para vender.");
                        player.closeInventory();
                        return;
                    }

                    val item = new ItemStack(product.getMaterial(), amount);
                    val meta = item.getItemMeta();
                    meta.setDisplayName(product.getName());
                    meta.setLore(product.getLore());
                    player.getInventory().removeItem(item);

                    economyManager.deposit(player, PlayerCoinType.COIN, price);
                    MessageUtils.sendColouredMessage(player, String.format("&a%d unidade(s) de &f%s &avendidas(s) por &f$%s&a.",
                            amount, MarketConfigCommand.getName(item), formatter.format(price)));
                    Main.getInstance().getProductManager().logTrade(player, product, TradeType.SELL);
                    //player.closeInventory();
                } else if (click.isRightClick()) {
                    if (product.getBuyPrice() <= 0)
                        return;

                    val amount = click.isShiftClick() ? product.getMaterial().getMaxStackSize() : product.getAmount();
                    val price = (((double) amount) / product.getAmount()) * product.getBuyPriceByPlayer(player);

                    if (economy == null || !economy.hasBalance(PlayerCoinType.COIN, price)) {
                        player.sendMessage(ChatColor.RED + "Você não tem saldo para fazer essa compra.");
                        player.closeInventory();
                        return;
                    }

                    if (product.getCategory() == Category.WAR && product.getEnergy() > 0 && Main.getInstance().getDatabase().getEnergyByPlayer(player) <= product.getEnergy()) {
                        player.sendMessage(ChatColor.RED + "Você não tem energia para fazer essa compra.");
                        player.closeInventory();
                        return;
                    }

                    if (!InventoryUtils.hasSpace(player.getInventory(), product.getMaterial(), product.getAmount())) {
                        player.sendMessage(ChatColor.RED + "Você não tem espaço no inventário.");
                        player.closeInventory();
                        return;
                    }

                    economyManager.withdraw(player, PlayerCoinType.COIN, price);

                    if (product.getCategory() == Category.WAR && product.getEnergy() > 0)
                        Main.getInstance().getDatabase().withdrawEnergyToPlayer(player, product.getEnergy());

                    val item = new ItemStack(product.getMaterial(), amount);
                    val meta = item.getItemMeta();

                    if (product.getName() != null)
                        meta.setDisplayName(product.getName());

                    if (product.getLore() != null)
                        meta.setLore(product.getLore());

                    item.setItemMeta(meta);
                    player.getInventory().addItem(item);
                    MessageUtils.sendColouredMessage(player, String.format("&a%d unidade(s) de &f%s &acomprada(s) por &f$%s&a.",
                            amount, MarketConfigCommand.getName(item), formatter.format(price)));
                    Main.getInstance().getProductManager().logTrade(player, product, TradeType.BUY);
                    //player.closeInventory();
                }
            }
        };
    }

    public static int countItems(Player player, Product product) {
        var items = player.getInventory().all(product.getMaterial()).values();

        items = items.stream()
                .filter(item -> item.getItemMeta().getDisplayName().equals(product.getName()))
                .collect(Collectors.toList());
        items = items.stream()
                .filter(item -> item.getItemMeta().getLore() == product.getLore())
                .collect(Collectors.toList());

        return items.stream().mapToInt(ItemStack::getAmount).sum();
    }
}
