package br.com.finalelite.market;

import br.com.finalelite.market.commands.MarketCommand;
import br.com.finalelite.market.commands.MarketConfigCommand;
import br.com.finalelite.market.commands.MarketOthersCommand;
import br.com.finalelite.market.utils.Database;
import br.com.finalelite.market.utils.ProductManager;
import br.com.finalelite.pauloo27.api.PauloAPI;
import br.com.finalelite.pauloo27.api.commands.CommandablePlugin;
import lombok.Getter;

import java.sql.SQLException;

public class Main extends CommandablePlugin {

    @Getter
    private static Main instance;

    @Getter
    private Database database;
    @Getter
    private ProductManager productManager;

    @Override
    public void onEnable() {
        instance = this;
        database = new Database(PauloAPI.getInstance().getEzSQL("mixedup"));
        try {
            database.connect();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        productManager = new ProductManager(database);
        productManager.clearCache();

        registerCommand(new MarketCommand());
        registerCommand(new MarketOthersCommand());
        registerCommand(new MarketConfigCommand());
    }
}
